SELECT  
	BIG.ASSESSMENT_YEAR AS Year,
	CAST(BIG.condo_strata_100 AS varchar) + ' / 100' AS [Strata],
	strata_100_minsp AS [Strata Minimum Sale Price],
	strata_100_medsp AS [Strata Median Sale Price],
	strata_100_maxsp AS [Strata Maximum Sale Price] 
FROM DTBL_CONDOSTRATA AS BIG
LEFT JOIN (
	SELECT *
	FROM DTBL_CONDOSTRATA_STATS100
) STATS100
ON STATS100.condo_strata_100 = BIG.condo_strata_100
WHERE PIN10 = {val1}
UNION ALL
SELECT  
	BIG.ASSESSMENT_YEAR AS Year,
	CAST(BIG.condo_strata_10 AS varchar) + ' / 10' AS [Strata],
	strata_10_minsp AS [Strata Minimum Sale Price],
	strata_10_medsp AS [Strata Median Sale Price],
	strata_10_maxsp AS [Strata Maximum Sale Price] 
FROM DTBL_CONDOSTRATA AS BIG
LEFT JOIN (
	SELECT *
	FROM DTBL_CONDOSTRATA_STATS10
) STATS10
ON STATS10.condo_strata_10 = BIG.condo_strata_10
WHERE PIN10 = {val1}