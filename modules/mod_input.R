mod_input_UI <- function(id) {
  ns <- NS(id)

  # This module handles the inputs on the sidebar, including the minimum
  # year slider, the PIN input text box, and the submit button
  tagList(
    rclipboardSetup(),

    # Input box for typing out a PIN, validated by server function
    textInput(
      ns("PIN"),
      label = "Enter a 10 or 14-digit PIN:",
      value = "04341060080000"
    ),

    # Submit button that triggers different reactive functions
    div(
      style = "display: flex",
      actionButton(
        ns("PIN_submit"),
        "Submit PIN",
        class = "sidebar-button",
        style = "margin-right: 6px"
      ),
      uiOutput(ns("clipboard"))
    )
  )
}


mod_input <- function(input, output, session) {
  ns <- session$ns
  
  # Create clipboard button
  output$clipboard <- renderUI({
    current_url <- paste0(
      session$clientData$url_protocol, "//",
      session$clientData$url_hostname,
      session$clientData$url_pathname
    )
    
    rclipButton(
      "clip",
      "Copy Link",
      paste0(current_url, "?pin=", input$PIN),
      icon("clipboard"),
      width = "60px"
    )
  })

  # Reactive element that takes the typed PIN as input and return a clean PIN
  # The clean pin has no whitespace and no dashes or separators, it is ONLY
  # length 10 or 14
  clean_pin_value <- reactive({
    clean_pin <- suppressWarnings(ccao::pin_clean(input$PIN))
    if (is.null(check_pin_value(clean_pin)) & str_length(clean_pin) == 10) {
      clean_pin <- str_pad(clean_pin, 14, "right", "0")
    }

    return(clean_pin)
  })

  # shinyFeedback observer that checks whether or not a typed PIN is valid
  # If not valid, the text argument will be displayed below the input box in red
  observeEvent(input$PIN, {
    req(clean_pin_value())
    
    # Disable the PIN submit button if input is invalid 
    if (is.null(check_pin_value(clean_pin_value()))) {
      shinyjs::enable(ns("PIN_submit"), asis = TRUE)
    } else {
      shinyjs::disable(ns("PIN_submit"), asis = TRUE)
    }
    
    # Display warning message on bad PIN
    if (!is.null(check_pin_value(clean_pin_value()))) {
      hideFeedback("PIN")
      showFeedbackDanger(
        inputId = "PIN",
        text = paste(
          "This is not a valid PIN!",
          "Please remove any letters and ensure",
          "the PIN is either 10 or 14-digits long"
        )
      )
    } else {
      hideFeedback("PIN")
      showFeedbackSuccess(inputId = "PIN")
    }

  })

  # Input observe for handling PIN inputs passed via a URL parameter
  # The PIN or pin parameters are pasted directly into the PIN input text box
  observe({
    query <- parseQueryString(session$clientData$url_search)
    query_pin <- c(query[["PIN"]], query[["pin"]])
    if (!is.null(query_pin)) {
      updateTextInput(session, "PIN", value = query_pin)
    }
  })

  # List of reactive values that are returned to the main shiny application
  # These values are passed to other modules, reactives, and queries
  output_lst <- list(
    pin_value = clean_pin_value,
    pin_submit = reactive({
      input$PIN_submit
    })
  )

  return(output_lst)
}
