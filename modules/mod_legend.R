mod_legend_UI <- function(id) {
  ns <- NS(id)

  tagList(
    htmlOutput(ns("text_pin_mryad")),
    br()
  )
}

mod_legend <- function(input, output, session, get_diag_lst) {
  ns <- session$ns
  
  ##### TARGET PIN #####
  
  output$text_pin_mryad <- renderText({
    req(is.null(check_diag_lst(get_diag_lst())))
    fmv_type <- ifelse(
      get_diag_lst()$diag_mryad_type == "cert",
      "Assessor Certified",
      str_to_title(get_diag_lst()$diag_mryad_type)
    )
    paste(
      "Using", 
      get_diag_lst()$diag_mryad_year, 
      fmv_type, 
      "Fair Market Value", 
      definition("fmv", placement="bottom")
    )
  })

}