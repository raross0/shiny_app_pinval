###### DATA EVENT HANDLERS #####

# These are handlers that run some arbitrary function when an event occurs.
# In our case, the app queries the database when a PIN is submitted and
# returns whichever dataset is specified in the ingest script

# Each handler is named after the dataframe or object that it returns.
# The handlers can then be passed to other server functions such as
# renderPlot() as if they are data frames or objects

# Reactives are used in plot and table code as functions. For example,
# when you need to use appeals data in a plot, you can type get_appeals_df()
# where a dataframe named appeals_df would normally exist

get_diag_lst <- eventReactive(
  pin_input$pin_submit(), {
    validate_no_msg(check_pin_value(pin_input$pin_value()))
    ingest_diag_lst(CCAODATA, pin_input$pin_value())
  }
)

get_appeals_df <- eventReactive(
  pin_input$pin_submit(), {
    validate_no_msg(check_diag_lst(get_diag_lst()))
    ingest_appeals_df(CCAODATA, get_diag_lst())
  }
)

get_comps_df <- eventReactive(
  pin_input$pin_submit(), {
    validate_no_msg(check_diag_lst(get_diag_lst()))
    ingest_comps_df(CCAODATA, get_diag_lst())
  }
)

get_assmnt_df <- eventReactive(
  pin_input$pin_submit(), {
    validate_no_msg(check_diag_lst(get_diag_lst()))
    ingest_assmnt_df(CCAODATA, get_diag_lst())
  }
)

get_condos_sales_df <- eventReactive(
  pin_input$pin_submit(), {
    validate_no_msg(check_diag_lst(get_diag_lst()))
    ingest_condo_sales_df(CCAODATA, get_diag_lst())
  }
)

get_unif_df <- eventReactive(
  pin_input$pin_submit(), {
    validate_no_msg(check_diag_lst(get_diag_lst()))
    ingest_unif_df(CCAODATA, get_diag_lst())
  }
)

get_h288s_df <- eventReactive(
  pin_input$pin_submit(), {
    validate_no_msg(check_diag_lst(get_diag_lst()))
    ingest_h288s_df(CCAODATA, get_diag_lst())
  }
)
